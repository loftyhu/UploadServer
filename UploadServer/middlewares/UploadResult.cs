using Newtonsoft.Json;

namespace UploadServer.middlewares
{
    public class UploadResult
    {
        public bool ok { get; set; }
        public string msg { get; set; }
        public string url { get; set; }

        public string toJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}